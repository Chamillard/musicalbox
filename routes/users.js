var express = require('express');
var router = express.Router();
var Account = require('../models/account');

// Affichage de la liste
router.get('/', async function(req, res, next) {
  await Account.findOne({"login" : "Admin"},
   function(err, result) {
    if (err) {
      console.log("pas ok");
    } else {
      if (!result) {
       // Creating one user.
       var admin = new Account ({
        login: 'Admin',
        passe: 'slam',
        role: 'Administrateur'
       });
 
       // Saving it to the database.
       admin.save(function (err) {if (err) console.log ('Erreur de sauvegarde !')});
      }
    }
  });

  var db = req.db;
  var collection = db.get('accounts');

  var listeUtilisateurs = await collection.find({});
  
  res.render('users', {
    "title" : "Liste des utilisateurs",
    "liste" : listeUtilisateurs
  });
});

module.exports = router;
