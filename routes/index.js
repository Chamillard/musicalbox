var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// Liste la vue des titres d'après le fichier JSon
router.get('/songs', function(req, res, next) {
  fs.readFile(path.join(__dirname,'../api/songs.json'),'utf8', function(err, songs) {
    if (err) { throw err; }
    liste = JSON.parse(songs);
    res.send(liste);
  });
});

// Liste un titre en fonction de son numéro
router.get('/songs/:num', function(req, res, next) {
  var numero = req.params.num;
  var tabMusiques = [];

  fs.readFile(path.join(__dirname, '../api/songs.json'), 'utf8', function (err, songs) {
    if (err) {
      throw err;
    }

    let resultat = JSON.parse(songs);
    let liste = resultat.songs;

    for (var i in liste) {
      tabMusiques.push(liste[i].titre);
    }

    if (numero<tabMusiques.length) {
      res.send("La musique : "+tabMusiques[numero]);    
    } else {
      res.send("Musique non existante !");
    }
  });
});

module.exports = router;
