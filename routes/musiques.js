var express = require('express');
var router = express.Router();
var path = require('path');
var fs = require('fs');
var ms = require('mediaserver');

// Liste des musiques via MongoDB
router.get('/', async function(req, res, next) {
    var db = req.db;
    var collection = db.get('titres');

    var listeMusiques = await collection.find({});
    var objResult = await collection.aggregate({"$group":{_id:"$genre",count:{$sum:1}}});
    var result = "";

    objResult.forEach(function (item, key) {
        result = result + item['_id'] + ":" + item['count'] + " ";
    });
    
    res.render('musiques', {
      "title" : "Liste des musiques",
      "liste" : listeMusiques,
      "sommes" : result
    });
});

/*
// Joue le morceau via un flux
router.get('/:titre', function(req, res, next) {
    var chanson = path.join(__dirname,'../raw',req.params.titre);
    ms.pipe(req,res, chanson+'.mp3');
});
*/

// Joue le morceau via une vue
router.get('/:titre', function(req, res, next) {
    var musique = "/raw/"+req.params.titre+'.mp3';

    res.render('player', { "title" : "Player",
        "chanson" : req.params.titre,
        "morceau" : musique   
    });
});

// Traitement pour la reception du fichier musical
router.post('/upload', async function(req, res, next) {
    try {
        // Si aucun fichier...
        if(!req.files) {
            // Retour du message d'erreur, en production cela ne se fait pas...
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            // Récupérération du fichier avec son "name" HTML
            let morceau = req.files.upload;
            
            // Déplacement du fichier à l'aide de la méthode mv()
            morceau.mv('./raw/' + morceau.name);

            // Affcihge de la réponse en mode console
            console.log({
                status: true,
                message: 'Fcichier uploaded',
                data: {
                    name: morceau.name,
                    mimetype: morceau.mimetype,
                    size: morceau.size
                }
            });

            // Ajout effectif et redirection vers la liste
            var db = req.db;
            var tmp = morceau.name;
            nom = tmp.substring(0,tmp.lastIndexOf("."));
            var auteur = req.body.auteur;
            var genre = req.body.genre;
            var collection = db.get('titres');
           
            collection.insert({
              "nom" : nom,
              "auteur" : auteur,
              "genre": genre
            }, function (err, doc) {
                if (err) {
                    console.log("Insertion non effectuée  !");
                } else {
                    console.log("Insertion effectuée  !");
                }    
                
                // Redirection vers la liste, donc vers une vue existante
                res.redirect("/musiques");
                
              });
        }
    } catch (err) {
        // Là, cela c'est mal passé...
        res.status(500).send(err);
    }
}); 

module.exports = router;